#include <iostream>
#include <string>
#include <wrl.h>
#include <cstring>

class A {

public:
    char *znak;

    A(const std::string &text)
            : znak(new char[text.size() + 1]) {
        memcpy(znak, text.c_str(), text.size() + 1);
    }


    A(A const &text)
            : znak(text.znak) {
        size_t size = strlen(text.znak) + 1;
        znak = new char[size];
        memcpy(znak, text.znak, strlen(text.znak) + 1);
    }


    A(std::string &&text) {
        znak = text.znak
        znak.text = nullptr;
    };


    ~A() {
        delete[] znak;
    };

};